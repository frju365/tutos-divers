# Déploiement d'un site grâce à Gitlab

J'utilise dans ce tuto [framagit](https://framagit.org). Il permet d'utiliser un nom de domaine en frama.io (ex : julien.frama.io) et également pour ceux qui possèdent un nom de domaine personnel de l'utiliser. Framagit utilise Gitlab CE (application qu'on peut héberger sur un serveur), tout comme le gitlab de l'IUT.

**Alors pourquoi ne pas le faire directement sur le git de l'IUT ?**

Le "Gitlab" de l'IUT ne possède pas de machine permettant de déployer un site web ou de faire des tests unitaires (les machine .

Il existe alors plusieurs solutions pour déployer un site web grâce à git : 
- soit on possède une machine capable de le faire (local ou distante).
- soit on le fait grâce à un service en ligne

Ici, je vais expliquer comment on fait pour déployer un site web simple sur le service Framagit. Github est très bien, mais j'ai encore de l'éthique et donc j'expliquerai la méthode pour Github après [^1]. 

Il faut suivre pour cela plusieurs étapes : 

## Sur Framagit (ou autres instances possédant Gitlab Pages)

1. S'inscrire sur Framagit

	Aller sur https://framagit.org/users/sign_in?redirect_to_referer=yes et inscrivez-vous.
    
2. Se créer un repo vide avec un nom en **.frama.io** (très important !!).
3. Uploader vos fichiers comme ce qui est indiqué dans le dépôt vide (ou bien créer un index.html pour tester).
Normalement, après cela vous devriez voir apparaître l'ensemble de vos sources (ou le fichier que vous avez créé).

4. Ajouter le script suivant et donner lui le nom de `.gitlab-ci.yml` [^2]

```yaml
pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
```

Exemple ici : https://framagit.org/spring1944/spring1944.frama.io/blob/master/.gitlab-ci.yml

5. Attendez le déploiement de votre site (cela peut prendre jusqu'à 15 minutes la première fois, 1 minute ensuite). À chaque `git push`, il déploiera le site à nouveau et donc le site sera constamment mis à jour. Pour aller sur votre site, vous écrivez juste le nom du site dans votre barre d'URL.

Remarque : 
La site ne prend en charge **que** les fichiers s'occupant du front. En effet, il ne doit y avoir aucun fichier php/java/nodejs... 


6. Pour le mettre en 'https' (site protégé) et qu'il ne l'est pas de suite , il faut aller vérifier dans "Settings > Pages" et cocher "Force HTTPS (requires valid certificates)".

7. (Pour ceux qui possèdent leur propre nom de domaine : Dans la même page "Settings > Pages", vous pouvez ajouter un nom de domaine personnalisé.)

## Sur Github

1. Créer un compte sur Github https://github.com/
2. Se créer un repo vide avec un nom en **.github.io** (très important !!).
3. Uploader vos fichiers comme ce qui est indiqué sur le dépôt vide (ou bien créer un index.html pour tester).
Normalement, après cela vous devriez voir apparaître l'ensemble de vos sources (ou votre fichier).
4. Aller dans l'onglet settings (du repo). Vérifier qu'on est bien dans l'onglet 'Options'. Aller un peu en bas sur la catégorie "Github Pages", choisir "Master Branch". Normalement, après ça, de la même façon, le déploiement prendra quelques minutes et à chaque `git push`, il redéploiera le site.

[^1]: https://github.com/1995parham/github-do-not-ban-us
[^2]: https://framagit.org/spring1944/spring1944.frama.io : Script sous licence GPL v3 provenant de ce projet pour lequel je travaille.


Sources : 
- Gitlab Pages : https://docs.gitlab.com/ee/user/project/pages/
- Github Pages : https://pages.github.com/
