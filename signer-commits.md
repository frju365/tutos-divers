# Signer ses commits

Il y a quelque chose de sympa quand on travaille sur des instances git et que j'aime bien : c'est la signature et le chiffrement des commits. En effet, on peut chiffrer ses propres commits et certifier que ce sont bien nos commits. Cela fait partie des bonnes pratiques d'internet et ce n'est pas très difficile à mettre en place.

### Comment signer ses commits ?

On va utiliser GnuPG qui est un programme permettant de créer des clefs de chiffrement, qui servent, en outre, à chiffre et signer des messages (ex : chiffrer un mail). Ce n'est pas tout à fait le même principe que la clef ssh, car la clef ssh permet d'avoir accès à quelque chose de manière sécurisée : serveur, dossier distant... GPG permet juste de chiffrer des messages de manière asymétrique [^1]. 

1. On va générer un clef de chiffrement : 
```bash
gpg --default-new-key-algo rsa4096 --gen-key
```

La commande va demander différentes informations. Il vaut mieux rentrer au moins l'info à propos de l'adresse mail. Il vaut mieux aussi indiquer que la clef n'expire jamais.

2. On va ensuite lister l'ensemble des clefs. Normalement, la dernière que nous avons créé (s'il y en a d'autres), c'est celle qui est présente tout en haut.

```bash
gpg --list-secret-keys --keyid-format LONG
```

Normalement, on repère que le nom de la clef est quelque chose comme `rsa4096/C392CE945FF5635A 2019-01-17 [SC]`. Ici le chiffre que je dois retenir c'est `C392CE945FF5635A`.

3. Ensuite, il va falloir, comme ssh, obtenir la clef publique. Pour cela, il faut faire : 

```bash
gpg --armor --export C392CE945FF5635A
```

Il va falloir copier l'ensemble du résultat obtenu.

4. Il faut alors l'ajouter sur notre instance Git préférée.
* Avec Gitlab (ou Framagit ou Git de l'IUT) : 
    * Il faut cliquer sur notre avatar en haut à droite et cliquer sur settings.
	    * À gauche, il devrait y avoir un truc comme "GPG keys".
	    * Il faudra ensuite juste copier/coller le résultat qu'on a obtenu avec la dernière commande du terminal et cliquer sur "Ajouter".
	 * Avec Github :
    *  Il faut cliquer sur notre avatar en haut à droite et cliquer sur settings.
    *  À gauche, il faut cliquer sur "SSH and GPG keys"
    *  Cliquer sur "New GPG key" et copier/coller le résultat de la commande de la même façon que précédemment.

5. Tout est terminé. Maintenant, il faut activer par défaut la signature de nos commits.

```bash
git config --global commit.gpgsign true
git config --global user.signingkey C392CE945FF5635A
```

Maintenant, à chaque commit, on nous demandera le mot de passe pour la clef, et celui-ci sera signé et chiffré.

Sinon, on peut chiffrer commit par commit avec l'option `-S`.
Ex :
```bash
git commit -S -m "Init"
```

(Attention à ne pas oublier le `git add .` avant et le  `git push` après)

6. (Pour vérifier le fonctionnement *correct* du chiffrement) : Il suffit d'aller voir sur l'instance git notre dernier commit (ou bien directement la liste de tout nos commits) et un petit badge vert indique "Verified".

Remarque : 
* Si on ne voit pas "verified", mais plutôt le contraire, c'est que l'adresse mail indiquée n'est pas connue de l'instance Git. Dans ce cas-là, il faut que l'instance vérifie notre adresse mail : Il faut aller dans "Settings > Emails" et ajouter l'adresse mail.
* La signature et le chiffrement n'empêchent pas d'avoir des commits non signés. La signature et le chiffrement s'entendent plutôt comme une **fonctionnalité** de git, que comme une *nécessité*.


[^1]: Le chiffrement asymétrique consiste en deux étapes. Vous possédez une clef privée et une clef publique. La clef publique va servir à chiffrer le message tandis que la clef privée va servir à le déchiffrer. C'est pour cette raison que l'on donne notre clef publique au destinatire : il va pouvoir chiffrer le message avec notre clef publique, et nous, de notre côté, on va pouvoir le déchiffrer grâce à notre clef privée (et lui, de même, quand on lui enverra un message). Pour plus d'informations : cf. https://fr.wikipedia.org/wiki/Cryptographie_asym%C3%A9trique


Source : 
- Configuration de git : https://git-scm.com/book/fr/v2/Personnalisation-de-Git-Configuration-de-Git
- Signature et chiffrement des commits : https://framagit.org/help/user/project/repository/gpg_signed_commits/index.md
- Infos intéressantes à propos déjà du vocabulaire sur la cryptographie : https://chiffrer.info/ (je suis sûr d'avoir fait des erreurs :D )
- Le chiffrement : https://www.leblogduhacker.fr/comment-fonctionne-le-chiffrement/
